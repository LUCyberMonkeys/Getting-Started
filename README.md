Here lie the resources you SHOULD look at or install before you attend a meeting.  It's highly recommended, you most likely won't be able to participate without them.

Hint: start with the wiki.

Also: if you missed the first couple of meetings and would like to see what you missed, mosey on over to the Meeting-Notes project, it will have notes on what you missed.